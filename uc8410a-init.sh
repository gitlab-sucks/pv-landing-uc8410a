#!/bin/bash

APP_SYS=("apt")
APP_VC=("python3" "wget" "tar" "xz" "chpasswd" "chown" "cp")
APT_SOURCES=/etc/apt/sources.list
APT_BPO_NOCHECKS=/etc/apt/apt.conf.d/04-bpo-nochecks

VC_ARCH="volcano.tar.xz"
VC_URL="https://gitlab.com/gitlab-sucks/pv-landing-uc8410a/uploads/e48c1c04fd823e8bf1b38ae83c60df93/volcano.tar.xz"
VC_PRJDIR=/home/volcano/project
VC_TMPDIR=/tmp/volcano-install

LOG_RED_='\033[1;31m'
LOG_YELLOW_='\033[1;33m'
LOG_NOCOL_='\033[0m' # No Color

log_err()
{
    echo -e "${LOG_RED_}Error: $1 ${LOG_NOCOL_}" 
}

log_warn()
{
    echo -e "${LOG_YELLOW_}Warning: $1 ${LOG_NOCOL_}" 
}

log_info()
{
    echo -e "${LOG_NOCOL_}$1 ${LOG_NOCOL_}" 
}

fail()
{
  log_err "$@"
  exit 1
}

init_system()
{
  log_info "Start system init..."
  log_info "Checking requirements..."

  if [ $UID -ne 0 ]; then
    fail "Please run as root"
  fi

  # Проверить, что все необходимые утилиты у нас есть
  for app in "${APP_SYS[@]}"; do
    if ! [ -x "$(command -v "${app}")" ]; then
      fail "'${app}' is not installed"
    fi
  done

  # Настроить списки репозиториев. Сохранив копию оригинала
  log_info "Setting up APT..."
  if [ -f "${APT_SOURCES}" ]; then
    if [ ! -f "${APT_SOURCES}.orig" ]; then
      cp ${APT_SOURCES} ${APT_SOURCES}.orig
    fi
  fi

(
cat << MSG
deb http://debian.moxa.com/debian/ jessie main

deb http://ftp.us.debian.org/debian/ jessie main contrib non-free
deb-src http://ftp.us.debian.org/debian/ jessie main contrib non-free

deb http://security.debian.org/ jessie/updates main contrib non-free
deb-src http://security.debian.org/ jessie/updates main contrib non-free

deb http://archive.debian.org/debian jessie-backports main 
MSG
) > ${APT_SOURCES}

  echo 'Acquire::Check-Valid-Until "false";' > ${APT_BPO_NOCHECKS}

  # Обновить сведения о пакетах
  log_info "Updating packages info..."
  apt update -y || fail "Can't update packages info"

  # Установить доп. пакеты
  log_info "Installing packages..."
  apt install -y python3 python3-setuptools python3-twisted libssl1.0.0 socat || fail "Can't install packages"

  log_info "Done"
}

init_volcano()
{
  log_info "Start Volcano init..."
  log_info "Checking requirements..."

  if [ $UID -ne 0 ]; then
    fail "Please run as root"
  fi

  # Проверить, что все необходимые утилиты у нас есть
  for app in "${APP_VC[@]}"; do
    if ! [ -x "$(command -v "${app}")" ]; then
      fail "'${app}' is not installed"
    fi
  done

  rm -rf ${VC_TMPDIR}
  mkdir -p ${VC_TMPDIR} || fail "Can't create working directory"

  # Если архив с VC уже загружен, то распаковываем. Иначе пробуем скачать 
  if [ ! -f ${VC_ARCH} ]; then
    log_info "Downloading Volcano..."
    wget -O ${VC_TMPDIR}/${VC_ARCH} ${VC_URL} || fail "Can't download Volcano"
  else
    log_info "Copying Volcano..."
    cp ${VC_ARCH} ${VC_TMPDIR}/${VC_ARCH} || fail "Can't create Volcano copy"
  fi

  log_info "Unpacking Volcano..."
  # Распаковать весь архив
  tar xJf ${VC_TMPDIR}/${VC_ARCH} -C ${VC_TMPDIR} > /dev/null || fail "Can't unpack Volcano"

  # Установить модули. Порядок важен. Изначально за ним следил pip...
  # Но pip'а здесь нет. Мы за него, т.к. других вариантов распространения 
  # VC не предпологал
  # volcano-core
  prev=$(pwd)
  log_info "Installing volcano-core..."
  (cd ${VC_TMPDIR}/volcano-core/deps/volcano-general &&
  python3 setup.py install > /dev/null &&
  cd ${VC_TMPDIR}/volcano-core/deps/volcano-test &&
  python3 setup.py install > /dev/null &&
  cd ${VC_TMPDIR}/volcano-core  &&
  python3 setup.py install > /dev/null) ||  fail "Can't install volcano-core"

  # volcano-twistedclient
  log_info "Installing volcano-twistedclient..."
  (cd ${VC_TMPDIR}/volcano-twistedclient &&
  python3 setup.py install > /dev/null) || fail "Can't install volcano-twistedclient"

  # volcano-mbsrv
  log_info "Installing volcano-mbsrv..."
  (cd ${VC_TMPDIR}/volcano-mbsrv &&
  python3 setup.py install > /dev/null) || fail "Can't install volcano-mbsrv"

  # volcano-web
  log_info "Installing volcano-web..."
  (cd ${VC_TMPDIR}/volcano-web && 
    python3 setup.py install > /dev/null) || fail "Can't install volcano-web"

  # volcano-poller
  log_info "Installing volcano-poller..."
  (cd ${VC_TMPDIR}/volcano-poller && 
  python3 setup.py install > /dev/null) || fail "Can't install volcano-poller"

  cd "${prev}" || fail "Can't select directory"

  grep ^volcano /etc/passwd > /dev/null
  res=$?
  if [ ${res} -ne 0 ]; then
    log_info "Adding user..."
    useradd -m -g users -G systemd-journal,plugdev,netdev,sudo,dip -s /bin/bash volcano || fail "Can't add user"
    echo "volcano:volcano" | chpasswd || fail "Can't change user's password"
  fi

  log_info "Adding services..."
  cp -r ${VC_TMPDIR}/systemd/* /etc/systemd/system || fail "Can't install services"
  systemctl daemon-reload

  rm -rf ${VC_TMPDIR}
  log_info "Done"
}

init_project()
{
  log_info "Prepare project..."
  log_info "Checking requirements..."

  if [ $UID -ne 0 ]; then
    fail "Please run as root"
  fi

  # Проверить, что все необходимые утилиты у нас есть
  for app in "${APP_VC[@]}"; do
    if ! [ -x "$(command -v "${app}")" ]; then
      fail "'${app}' is not installed"
    fi
  done

  rm -rf ${VC_TMPDIR}
  mkdir -p ${VC_TMPDIR} || fail "Can't create working directory"

  # Если архив с VC уже загружен, то распаковываем. Иначе пробуем скачать 
  if [ ! -f ${VC_ARCH} ]; then
    log_info "Downloading Volcano..."
    wget -O ${VC_TMPDIR}/${VC_ARCH} ${VC_URL} || fail "Can't download Volcano"
  else
    log_info "Copying Volcano..."
    cp ${VC_ARCH} ${VC_TMPDIR}/${VC_ARCH} || fail "Can't create Volcano copy"
  fi

  log_info "Unpacking Volcano..."
  # Распаковать весь архив
  tar xJf ${VC_TMPDIR}/${VC_ARCH} -C ${VC_TMPDIR} > /dev/null || fail "Can't unpack Volcano"

  # Скопировать и установить права
  if [ -d ${VC_PRJDIR} ]; then
    log_warn "${VC_PRJDIR} already exists. It will be overwritten"
    rm -rf ${VC_PRJDIR}
  fi
  cp -rf ${VC_TMPDIR}/demo-project /home/volcano/project || fail "Can't copy project to home directory"
  chown -R volcano:users /home/volcano/project || fail "Can't set project owner"

  log_info "Setting up services..."
  systemctl stop volcano-core &> /dev/null
  systemctl stop volcano-mbsrv &> /dev/null
  systemctl stop volcano-poller &> /dev/null
  systemctl stop volcano-port1-setinterface &> /dev/null
  systemctl stop volcano-port1-socat &> /dev/null

  systemctl enable volcano-core
  systemctl enable volcano-mbsrv
  systemctl enable volcano-poller
  systemctl enable volcano-port1-setinterface
  systemctl enable volcano-port1-socat

  systemctl start volcano-core
  systemctl start volcano-mbsrv
  systemctl start volcano-poller
  systemctl start volcano-port1-setinterface
  systemctl start volcano-port1-socat

  rm -rf ${VC_TMPDIR}
  log_info "Done"
}

usage()
{
  echo "Moxa UC8410A: landing tools"
  echo ""
  echo "Usage:"
  echo "  uc8410a-init.sh mode"
  echo ""
  echo "Modes:"
  echo "  system - install system components"
  echo "  volcano - install volcano components"
  echo "  project - install project"
}

init()
{
  argc="$#"
  if [ $argc -eq 0 ]; then
    usage
    exit 1
  fi
  for arg in "$@"; do
    if [ "${arg}" == "system" ]; then
      init_system
    elif [ "${arg}" == "volcano" ]; then
      init_volcano
    elif [ "${arg}" == "project" ]; then
      init_project
    else
      fail "Unknown mode '${arg}'"
    fi
  done
}

init "$@"
